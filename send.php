<?php
/* Set e-mail recipient */
$myemail  = "info@intercitygreenxpress.com";

/* Check all form inputs using check_input function */
$fname = check_input($_POST['fname']);
$lname = check_input($_POST['lname']);
$email  = check_input($_POST['email']);
$phone   = check_input($_POST['phone']);
$menu   = check_input($_POST['menu']);
$message = check_input($_POST['message']);
$subject   = "New Enquiry From Customer";




/* Let's prepare the message for the e-mail */
$message = "Hello!

Your contact form has been submitted by:

First Name: $fname
Last Name : $lname
E-mail: $email
Phone : $phone
Department:  $menu
Message : $message


End of message
";

/* Send the message using mail() function */
mail($myemail, $subject, $message);

/* Redirect visitor to the thank you page */
header('Location: contact.html');
exit();

/* Functions we used */
function check_input($data, $problem='')
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    if ($problem && strlen($data) == 0)
    {
        show_error($problem);
    }
    return $data;
}

function show_error($myError)
{
?>
    <html>
    <body>

   
    <?php echo $myError; ?>

    </body>
    </html>
<?php
exit();
}
?>
